#!/bin/bash

username=$(whoami)
date=$(date +"hoje é dia %d de %B de %Y, %A.")
echo "Bom dia," $username $date "\n";

poesia=(
"\n\033[32mNeologismo\n"
"Beijo pouco, falo menos ainda."
"Mas invento palavras"
"Que traduzem a ternura mais funda"
"E mais cotidiana."
"Inventei, por exemplo, o verbo teadorar."
"Intransitivo:"
"Teadoro, Teodora.\033[0m")

for verso in ${!poesia[@]}; do
  echo -e ${poesia[$verso]};
  sleep 1
done
